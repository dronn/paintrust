use std::fs::File;
use std::io::Write as _;
use std::io::Error;
use crate::graphicsObject::*;
use std::fmt::Write as _;
use crate::egui::Color32;

pub fn write_svg( graphic_objects: &mut [GraphicsObject], strokes: &mut Vec<Vec<(f32, f32)>> )-> Result<(), Error>
{
	let path = "tst.svg";
    let mut output = File::create(path)?;
    let mut line = format!("");
    
	let svgHeader=
	"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n\
	<svg\n\
	   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n\
	   xmlns:cc=\"http://creativecommons.org/ns#\"\n\
	   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n\
	   xmlns:svg=\"http://www.w3.org/2000/svg\"\n\
	   xmlns=\"http://www.w3.org/2000/svg\"\n\
	   viewBox=\"0 0 1920 1080\"\n\
	   version=\"1.1\"\n\
	   id=\"svg2\"\n\
	   height=\"1080\"\n\
	   width=\"1920\">\n\
	  <defs\n\
	     id=\"defs4\" />\n\
	  <metadata\n\
	     id=\"metadata7\">\n\
	    <rdf:RDF>\n\
	      <cc:Work\n\
	         rdf:about=\"\">\n\
	        <dc:format>image/svg+xml</dc:format>\n\
	        <dc:type\n\
	           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n\
	        <dc:title></dc:title>\n\
	      </cc:Work>\n\
	    </rdf:RDF>\n\
	  </metadata>\n\
	  <g\n\
	     id=\"layer1\" >\n";
	let mut counter = 0;
	let mut first = true;
	line += format!("{svgHeader}").as_str();
	
	 for s in strokes{
			counter = counter + 1;
			
			first=true;
			for c in s {
				let p0 = c.0;
				let p1 = c.1;
				
				if first {
					line += format!("<path\n       id=\"path{counter}\"\n       d=\"M {p0}, {p1} L ").as_str();
					line += format!("{p0},{p1} ").as_str();
					first=false
				}
				else
				{
					line += format!("{p0},{p1} ").as_str();
				}
				
			}
		let go = graphic_objects[counter-1];
		let alpha = go.alpha;
		let radius = go.pen_radius*10.0;
		let colour_str=colourString(go.color);
		if !first
		{
			if go.ObjectType == VectorObjectIdentifier::LOOP
			{
				line += format!("\"\n       style=\"opacity:{alpha:.3};fill:{colour_str};fill-opacity:{alpha:.3};stroke:none;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n").as_str();
			}
			else
			{
				line += format!("\"\n       style=\"opacity:{alpha:.3};fill:none;fill-opacity:0;stroke:{colour_str};stroke-width:{radius};stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n").as_str();

			}
		}
	}
	line += format!(" </g>\n </svg>\n").as_str();
	return write!(output, "{}", line);
}

pub fn encode_hex(bytes: &[u8]) -> String {
    let mut s = String::with_capacity(bytes.len() * 2);
    for &b in bytes {
        write!(&mut s, "{:02x}", b).unwrap();
    }
    s
}

pub fn colourString(color: Color32) -> String
{
	let mut bytes : [u8;3] = [0; 3];
	bytes[0] = color.r() as u8;
	bytes[1] = color.g() as u8;
	bytes[2] = color.b() as u8;
	let mut s = "#".to_owned() + &encode_hex(&bytes);
	s
}
