use crate::graphicsObject::*;
use crate::graphicsObject::VectorObjectIdentifier;
use byteorder::{LittleEndian};
use std::fs::File;
use std::io::Read;
use crate::egui::Color32;
use byteorder::ByteOrder;
use std::io::Write;

const HEADER_SIZE: usize =  3;
const STROKE_HEADER_SIZE: usize = 9;
const TYPE_OFFSET: usize  = 0;
const RADIUS_OFFSET: usize  = 1;
const FINGERS_OFFSET: usize  = 2;
const VARIANCE_OFFSET: usize  = 3;
const RED_OFFSET: usize  = 4;
const GREEN_OFFSET: usize  = 5;
const BLUE_OFFSET: usize  = 6;
const ALPHA_OFFSET: usize  = 7;
const COORD_SIZE_OFFSET: usize  = 8;

const RADIUS_ADJUSTMENT_FACTOR: u16 = 10;

/*
struct strokesHeader
{
	uint16_t type;
	uint16_t radius;
	uint16_t fingers;
	uint16_t variance;
	uint16_t red;
	uint16_t green;
	uint16_t blue;
	uint16_t alpha;
	uint16_t coord_size;
};

 */

pub fn loadBinary(filename : String, bin: &mut Vec<u8>)  {
    let mut file = File::open(filename).expect("Failed to open file");
    file.read_to_end(bin).expect("Failed to read file");

}
pub fn write_binary (bin16: &Vec<u16>) -> std::io::Result<()>
{
	let mut file = File::create("test_b")?;
	unsafe{
		let _ = file.write_all(bin16.align_to::<u8>().1);
	}
	Ok(())
}

pub fn populateArray (graphic_objects: &mut Vec <GraphicsObject>, strokes: &mut Vec<Vec<(f32, f32)>>, bin16: &mut Vec<u16> )
{
	let mut overall_size = HEADER_SIZE + graphic_objects.len() * STROKE_HEADER_SIZE;
	let mut coord_num = 0;
	
	for x in & mut *strokes
	{
		for y in x
		{
			coord_num += 1;
		}
	}
	
	overall_size += coord_num * 2;
	
	bin16.resize(overall_size, 0);
	
	// write header
	bin16[0] = 1227;
	bin16[1] = graphic_objects.len() as u16;
	bin16[2] = overall_size as u16;
	let mut index = 2;
	let mut graphic_object_counter =0;
	
	for x in graphic_objects
	{
		index +=1;
		if x.ObjectType == VectorObjectIdentifier::PEN
		{
			bin16[index] = 0;
		}
		else
		{
			bin16[index] = 1;
	
		}
		index += 1;
		bin16[index] = (x.pen_radius * 100.0) as u16;
		index += 1;
		bin16[index] = x.fingers as u16;
		index += 1;
		bin16[index] = (x.variance * 100.0) as u16;
		index += 1;
		bin16[index] = (x.color.r() as u16 * 256) as u16;
		index += 1;
		bin16[index]= (x.color.g() as u16 * 256) as u16;
		index += 1;
		bin16[index] = (x.color.b() as u16 * 256) as u16;
		index  += 1;
		bin16[index] = (x.alpha * 100.0) as u16;
		index += 1;
		bin16[index] = strokes[graphic_object_counter].len() as u16;
		graphic_object_counter += 1;
	}
	
	for stroke in &mut *strokes
	{
		for coord in stroke
		{
			if index + 2 < bin16.len()
			{
				index += 1;
				bin16[index]= coord.0 as u16;
				index += 1;
				bin16[index]= coord.1 as u16;
			}
			else
			{
				println!("write bin out of bounds");
			}
		}
	}
	
	
}

pub fn populateHistory( graphic_objects: &mut Vec <GraphicsObject>, strokes: &mut Vec<Vec<(f32, f32)>>, bin: &Vec<u8> )
{
	if bin.len() > 3
	{
		
		let mut coord_sizes = Vec::new();
		let mut bin16 = Vec::<u16>::new();
		bin16.resize(bin.len()/2, 0);

		let l = bin.len();
		LittleEndian::read_u16_into(bin, &mut bin16);
		
		let len = bin16[1];
		println!("loading {len} strokes");
		
		for i in 0..len as usize
		{
			let base_index = HEADER_SIZE+ i*STROKE_HEADER_SIZE as usize;
			let mut index = base_index + TYPE_OFFSET;
			let mut obj_type = VectorObjectIdentifier::PEN;
			if bin16[index] != 0
			{
				obj_type = VectorObjectIdentifier::LOOP
			}
			
			index = base_index + RADIUS_OFFSET;
			let radius = bin16[index];
			index = base_index + FINGERS_OFFSET;
			let fingers = bin16[index];
			index = base_index + VARIANCE_OFFSET;
			let variance = bin16[index];
			index = base_index + GREEN_OFFSET;
			let green = bin16[index] /256;
			index = base_index + BLUE_OFFSET;
			let blue = bin16[index] /256;
			index = base_index + RED_OFFSET;
			let red = bin16[index] /256;
			index = base_index + ALPHA_OFFSET;
			let alpha = bin16[index];
			index = base_index + COORD_SIZE_OFFSET;
			let coord_size= bin16[index];
			
			coord_sizes.push(coord_size);
			let color = Color32::from_rgb_additive(red as u8, green as u8, blue as u8);
			let true_color = color.additive();
			let obj = GraphicsObject {
						ObjectType: obj_type,
						pen_radius: (radius / RADIUS_ADJUSTMENT_FACTOR) as f32 /100.0,
						fingers: fingers as i32,
						variance: variance as f32 /100.0,
						color: true_color,
						alpha: alpha as f32 /100.0,
					};
			graphic_objects.push(obj.clone());
		}
		let mut start_pos = HEADER_SIZE+STROKE_HEADER_SIZE*len as usize;
		
		for x in 0.. coord_sizes.len() 
		{
			let mut coords = Vec::new();
			
			for i in 0.. coord_sizes[x] -1
			{
				let index_x = (i*2) as usize;
				let index_y = (i*2+1) as usize;
				
				if start_pos + index_y < bin16.len()
				{
					let coord_x = bin16[start_pos + index_x] as u16;
					let coord_y = bin16[start_pos + index_y] as u16;
					let coord = (coord_x as f32, coord_y as f32);
					coords.push(coord);
				}
				else
				{
					println!("out of bounds {x} ");
					print!("start: {start_pos} ");
					print!("index_y {index_y} ");
					print!("i: {i} ");
					println!(" {len}");
				}
			}
			// 2 coords
			start_pos = start_pos + ((coord_sizes[x] as u16 )*2) as usize;
			strokes.push(coords.clone());
		}
	}
}
