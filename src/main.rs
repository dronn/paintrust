#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release
use raqote::*;
use eframe::egui;
use crate::egui::Key;
use crate::egui::PointerButton;
use crate::egui::color_picker::color_picker_color32;
use crate::egui::color_picker::Alpha;
use crate::egui::Color32;
mod graphicsObject;
use crate::graphicsObject::*;
use crate::graphicsObject::VectorObjectIdentifier;
mod svgWriter;
mod binaryFormat;
use crate::svgWriter::*;
use crate::binaryFormat::*;
use euclid::*;
use std::env;

const WIDTH: usize = 1900;
const HEIGHT: usize = 1080;

fn main() -> Result<(), eframe::Error> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(WIDTH as f32, HEIGHT as f32)),
        ..Default::default()
    };
    
    let args: Vec<String> = env::args().collect();
    let mut file = "".to_string();
    if args.len() > 1
    {
		file = args[1].clone();
	}

	let app = MyApp::new(file);
	
    eframe::run_native(
        "Path Paint",
        options,
        Box::new(|cc| {
            Box::new(app)
        }),    )
    
}


struct MyApp {
	bin : Vec<u8>,
	bin16: Vec<u16>,
	graphicObjects : Vec<GraphicsObject>,
	strokes : Vec<Vec<(f32,f32)>>,
	obj : GraphicsObject,
	dt: DrawTarget,
	dt_old : DrawTarget,
	coords : Vec<(f32,f32)>,
	isPenDown: bool,
	tool : VectorObjectIdentifier,
	pen_radius: f32,
	variance: f32,
	alpha: f32,
	size: (i32, i32),
	init: bool,
	loadBin: bool,
	rerenderCounter: usize,
	ui_shown: bool,
	ui_tool_loop: bool,
	file: String,
	colour: Color32,
	additive_colour: Color32,
	}

impl Default for MyApp {
    fn default() -> Self {
        Self {
			graphicObjects : Vec::new(),
			strokes : Vec::new(),
			bin : Vec::new(),
			bin16 : Vec::new(),
			obj : GraphicsObject {
				ObjectType: VectorObjectIdentifier::LOOP,
				pen_radius: 0.1,
				fingers: 1,
				variance: 0.0,
				color: Color32::BLACK,
				alpha: 0.9,
			},
			dt: DrawTarget::new(WIDTH as i32, HEIGHT as i32),
			dt_old : DrawTarget::new(WIDTH as i32, HEIGHT as i32),
			coords : Vec::new(),
			isPenDown : false,
			tool : VectorObjectIdentifier::PEN,
			pen_radius : 0.1,
			variance: 0.0,
			alpha : 0.9,
			size: (WIDTH as i32,HEIGHT as i32),
			init: false,
			loadBin: false,
			rerenderCounter: 0,
			ui_shown: false,
			ui_tool_loop: false,
			file: "".to_string(),
			colour : Color32::BLACK,
			additive_colour : Color32::BLACK,
        }
    }
}

impl MyApp{
	fn new (filename: String) -> Self
	{
		Self {
			graphicObjects : Vec::new(),
			strokes : Vec::new(),
			bin : Vec::new(),
			bin16 : Vec::new(),
			obj : GraphicsObject {
				ObjectType: VectorObjectIdentifier::LOOP,
				pen_radius: 0.1,
				fingers: 1,
				variance: 0.0,
				color: Color32::BLACK,
				alpha: 0.9,
			},
			dt: DrawTarget::new(WIDTH as i32, HEIGHT as i32),
			dt_old : DrawTarget::new(WIDTH as i32, HEIGHT as i32),
			coords : Vec::new(),
			isPenDown : false,
			tool : VectorObjectIdentifier::PEN,
			pen_radius : 0.1,
			variance: 0.0,
			alpha : 0.9,
			size: (WIDTH as i32,HEIGHT as i32),
			init: false,
			loadBin: false,
			rerenderCounter: 0,
			ui_shown: false,
			ui_tool_loop: false,
			file: filename,
			colour : Color32::BLACK,
			additive_colour : Color32::BLACK,
        }
	}
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
	egui::CentralPanel::default().show(ctx, |ui| {
			if !self.init
			{
				self.dt_old.clear(SolidSource::from_unpremultiplied_argb(0xff, 0xff, 0xff, 0xff));
				self.init = true;

				if self.file != "".to_string()
				{
					self.loadBin = true;
				}

				if self.loadBin
				{
					loadBinary(self.file.clone(), &mut self.bin);
					populateHistory(&mut self.graphicObjects, &mut self.strokes, &mut self.bin);
				}
			}
			let pointer = ui.input(|i| i.pointer.latest_pos());
			let mousePressed = ui.input(|i| i.pointer.button_pressed(PointerButton::Primary));
			let mouseReleased = ui.input(|i| i.pointer.button_released(PointerButton::Primary));
			let mouseContext = ui.input(|i| i.pointer.button_pressed(PointerButton::Middle));
			
			
			if mousePressed && ! self.isPenDown && !self.ui_shown
			{
				self.additive_colour = self.colour.additive();
				self.isPenDown = true;
						self.obj = GraphicsObject {
						ObjectType: self.tool,
						pen_radius: self.pen_radius,
						fingers: 1,
						variance: 0.0,
						color: self.colour.additive(),
						alpha: self.alpha,
					};
			}
			if !self.loadBin
			{
				if mouseReleased && !self.ui_shown
				{
					if self.isPenDown
					{
						let targetBox = Box2D::new(
							Point2D::new(0 as i32, 0 as i32),
							Point2D::new(self.size.0 as i32, self.size.1 as i32),
							);
							
						self.dt_old.copy_surface(&self.dt, targetBox, Point2D::new(0 as i32, 0 as i32));
						self.strokes.push(self.coords.clone());
						self.graphicObjects.push(self.obj.clone());
						self.coords.truncate(0);
					}
					self.isPenDown = false;
				}
				
				if !self.ui_shown
				{
					let targetBox = Box2D::new(
					Point2D::new(0 as i32, 0 as i32),
					Point2D::new(self.size.0 as i32, self.size.1 as i32),
					);
					self.dt.copy_surface(&self.dt_old, targetBox, Point2D::new(0 as i32, 0 as i32));
				}
				else
				{
					ui.horizontal(|ui|{
					color_picker_color32(ui, &mut self.colour,  Alpha::Opaque);

					ui.vertical(|ui|{
						ui.add(egui::Slider::new(&mut self.pen_radius, 0.1..=5.0).text("Pen Radius"));
						ui.add(egui::Slider::new(&mut self.alpha, 0.1..=1.0).text("Alpha"));
						ui.add(egui::Slider::new(&mut self.variance, 0.0..=1.0).text("Variance"));
						ui.add(egui::Checkbox::new(&mut self.ui_tool_loop, "Loop tool"));
						if ui.button("Save").clicked() {
							self.dt.write_png("tst.png").unwrap();
							let _ = write_svg(&mut self.graphicObjects, &mut self.strokes);
							populateArray( &mut self.graphicObjects, &mut self.strokes, &mut self.bin16);
							let _ = write_binary( &mut self.bin16);
							
						};
						if ui.button("OK").clicked() {
							self.ui_shown=false;
						};
						});
					});
				}
				
				if self.ui_tool_loop
				{
					self.tool =	VectorObjectIdentifier::LOOP;
				}
				else
				{
					self.tool =	VectorObjectIdentifier::PEN;
				}
			}
			
			match pointer{
				Some(pointer) =>
				{
					let x = pointer.x;

					let coord = (pointer.x, pointer.y);
					if self.isPenDown
					{
						self.coords.push(coord);
					}
				},
				None =>
				{
				}
			}
			
			if self.coords.len() > 2
				{
					let mut pb2 = PathBuilder::new();
					let mut state=0;
					for x in &self.coords {
						if state == 0
						{
							state = 1;
							//skip first coordinate
						}
						else if state == 1
						{
							pb2.move_to(x.0, x.1);
							state = 2;
						}
						else
						{
							pb2.line_to(x.0, x.1);
						}
					}
					let path2 = pb2.finish();
					if self.obj.ObjectType == VectorObjectIdentifier::PEN
					{
						self.dt.stroke(
						   &path2,
						   &Source::Solid(SolidSource::from_unpremultiplied_argb(
							   (self.obj.alpha *255.0) as u8,
								self.additive_colour.b() as u8,
								self.additive_colour.g() as u8,
								self.additive_colour.r() as u8,
						   )),
						   &StrokeStyle {
								width: self.obj.pen_radius*10.0,
								..StrokeStyle::default()
						   },
						   &DrawOptions::new(),
					   );
				   }
				   else
				   {
					   
					let draw_opt = DrawOptions :: new();
					self.dt.fill(&path2, &Source::Solid(SolidSource::from_unpremultiplied_argb(
							   (self.obj.alpha *255.0) as u8,
								self.additive_colour.b() as u8,
								self.additive_colour.g() as u8,
								self.additive_colour.r() as u8,
						   )),
								&DrawOptions::new(),);
					}
				}
				
			
				
			if self.loadBin && self.rerenderCounter < self.graphicObjects.len()
			{
				let targetBox = Box2D::new(
					Point2D::new(0 as i32, 0 as i32),
					Point2D::new(self.size.0 as i32, self.size.1 as i32),
					);
				if self.rerenderCounter == 0
				{
					self.dt.clear(SolidSource::from_unpremultiplied_argb(0xff, 0xff, 0xff, 0xff));
				}
				self.dt_old.copy_surface(&self.dt, targetBox, Point2D::new(0 as i32, 0 as i32));
				
				let mut rerenderer= PathBuilder::new();
				for x in 0 .. self.strokes[self.rerenderCounter].len() {

						if x == 0
						{
							rerenderer.move_to(self.strokes[self.rerenderCounter][x].0,
												self.strokes[self.rerenderCounter][x].1);
						}
						else
						{
							rerenderer.line_to(self.strokes[self.rerenderCounter][x].0,
												self.strokes[self.rerenderCounter][x].1);
						}
					}
					let rerendered = rerenderer.finish();
					
					if self.graphicObjects[self.rerenderCounter].ObjectType == VectorObjectIdentifier::PEN
					{
						self.dt.stroke(
						   &rerendered,
						   &Source::Solid(SolidSource::from_unpremultiplied_argb(
							   (self.graphicObjects[self.rerenderCounter].alpha *255.0) as u8,
								self.graphicObjects[self.rerenderCounter].color.b() as u8,
								self.graphicObjects[self.rerenderCounter].color.g() as u8,
								self.graphicObjects[self.rerenderCounter].color.r() as u8,
						   )),
						   &StrokeStyle {
								width: self.graphicObjects[self.rerenderCounter].pen_radius*10.0,
								..StrokeStyle::default()
						   },
						   &DrawOptions::new(),
					   );
				   }
				   else
				   {
					   
					let draw_opt = DrawOptions :: new();
					self.dt.fill(&rerendered, &Source::Solid(SolidSource::from_unpremultiplied_argb(
							   (self.graphicObjects[self.rerenderCounter].alpha *255.0) as u8,
								self.graphicObjects[self.rerenderCounter].color.b() as u8,
								self.graphicObjects[self.rerenderCounter].color.g() as u8,
								self.graphicObjects[self.rerenderCounter].color.r() as u8,
						   )),
								&DrawOptions::new(),);
					}
					
				self.rerenderCounter+=1;
				
				if self.rerenderCounter == self.graphicObjects.len()
				{
					self.loadBin = false;
				}
			}
				
			let pixels = self.dt.get_data_u8();

			let size = [WIDTH as _, HEIGHT as _];
			
			let image = egui::ColorImage::from_rgba_unmultiplied(
			size,
			pixels,
			);
			
			let handle = ctx.load_texture("texname", image, Default::default());
			let sized_image = egui::load::SizedTexture::new(handle.id(), egui::vec2(WIDTH as f32, HEIGHT as f32));

			ui.image(sized_image);

			if ctx.input(|i| i.key_released(Key::S)) {
				self.dt.write_png("tst.png").unwrap();
				let _ = write_svg(&mut self.graphicObjects, &mut self.strokes);
				populateArray( &mut self.graphicObjects, &mut self.strokes, &mut self.bin16);
				let _ = write_binary( &mut self.bin16);
				
			}
			else if ctx.input(|i| i.key_released(Key::F))
			{
				self.pen_radius += 0.1;
			}
			else if ctx.input(|i| i.key_released(Key::D))
			{
				if self.pen_radius > 0.2
				{
					self.pen_radius -= 0.1;
				}
			}
			else if ctx.input(|i| i.key_released(Key::X))
			{
				self.alpha += 0.1;
			}
			else if ctx.input(|i| i.key_released(Key::Y))
			{
				if self.alpha > 0.2
				{
					self.alpha -= 0.1;
				}
			}
			else if ctx.input(|i| i.key_released(Key::P)) {

				if self.tool == VectorObjectIdentifier::LOOP
				{
					self.tool = VectorObjectIdentifier::PEN
				}
				else
				{
					self.tool = VectorObjectIdentifier::LOOP
				}
				}
			else if ctx.input(|i| i.key_released(Key::Q)) {
				_frame.close();
				}
			else if ctx.input(|i| i.key_released(Key::C)) || mouseContext
			{
				self.ui_shown=true;
			}
			});
	}
	
}
