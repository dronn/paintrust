use crate::egui::Color32;
#[derive(Copy, Clone, PartialEq)]
pub enum VectorObjectIdentifier
{
	PEN,
	LOOP,
}

/*
 * object header containing type, radius, color and alpha of the graphic object
*/

#[derive(Copy, Clone)]
pub struct GraphicsObject
{
	pub ObjectType: VectorObjectIdentifier,
	pub pen_radius: f32,
	pub fingers: i32,
	pub variance: f32,
	pub color: Color32,
	pub alpha: f32
}
